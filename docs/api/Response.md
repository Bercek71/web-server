## HttpResponse class

The `HttpResponse` class in WebServer is used to construct and send HTTP responses to clients. It provides methods to set response headers, status codes, and content, as well as send the response back to the client.

### Public Functions

#### `getStatusCode()`

- **Description**: Get the status code of the response.
- **Returns**: The status code of the response.
- **Example**:
  ```cpp
  int statusCode = response.getStatusCode();
  ```
  
#### `status(int code, const std::string &message)`
- **Description**: Set the status code of the response.
- **Parameters**:
  - `code`: The status code to set.
  - `message`: The status message to include. (optional, defaults to the standard message for the code)
  - **Example**:
    ```cpp
    response.status(200, "OK");
    ```
    
#### `getHeaders() const`
- **Description**: Get the headers of the response.
- **Returns**: A map of headers included in the response.
- **Example**:
  ```cpp
  std::unordered_map<std::string, std::string> headers = response.getHeaders();
  ```

#### `setContentType(ContentType type)`
- **Description**: Set the content type of the response.
- **Parameters**:
  - `type`: The content type to set.
  - **Example**:
    ```cpp
    response.setContentType(ContentType::APPLICATION_JSON);
    ```

#### send(const std::string &body)
- **Description**: Send the response with the specified body content.
- **Parameters**:
  - `body`: The body content to include in the response.
  - **Example**:
    ```cpp
    response.send("Hello, World!");
    ```
    
#### sendFile(const std::string &path)
- **Description**: Send the response with the content of a file.
- **Parameters**:
  - `path`: The path to the file to include in the response.
  - **Example**:
    ```cpp
    response.sendFile("../assets/index.html");
    ```
    
#### sendJson(const json &data)
- **Description**: Send the response with JSON data.
- **Parameters**:
  - `data`: The JSON data to include in the response.
  - **Example**:
    ```cpp
    json data = {{"key", "value"}};
    response.sendJson(data);
    ```
    
#### `toString() const`
- **Description**: Get the response as a string.
- **Returns**: The response as a string.
- **Example**:
  ```cpp
  std::string responseString = response.toString();
  ```

#### `isLocked() const`
- **Description**: Check if the response is locked that means it has been sent.
- **Returns**: `true` if the response is locked, `false` otherwise.
- **Example**:
  ```cpp
  bool locked = response.isLocked();
  ```
  
#### `getBody()`
- **Description**: Get the body of the response.
- **Returns**: The body of the response.
- **Example**:
  ```cpp
  std::string body = response.getBody();
  ```
    