## Request Class

Request class represents a request to the server from a client. It contains information about the request such as the HTTP method, URL, headers, and body. The request object is passed to route handlers, allowing them to access and process the incoming request.

### Public Functions

#### `HttpRequest::HttpRequest(const std::string &method, const std::string &url, const std::unordered_map<std::string, std::string> &headers, const std::string &body)`
- **Description**: Constructor for the HttpRequest class.
- **Parameters**:
  - `method`: The HTTP method of the request (e.g., GET, POST).
  - `url`: The URL of the request.
  - `headers`: A map of headers included in the request.
  - `body`: The body of the request (if any).
  - **Returns**: An instance of the HttpRequest class.
  - **Example**:
    ```cpp
    HttpRequest request("GET", "/api/data", {{"Content-Type", "application/json"}}, "{\"key\": \"value\"}");
    ```
#### `std::string HttpRequest::getMethod() const`
- **Description**: Get the HTTP method of the request.
- **Returns**: The HTTP method of the request.
- **Example**:
  ```cpp
  std::string method = request.getMethod();
  ```
  
#### `std::string HttpRequest::getPath() const`
- **Description**: Get [path class](Path.md) of the request.
- **Returns**: The path of the request.
- **Example**:
  ```cpp
  Path path = request.getPath();
  ```
  
#### `std::string HttpRequest::getParam(const std::string &key) const`

- **Description**: Get the value of a query parameter from the request URL.
- **Parameters**:
  - `key`: The key of the query parameter.
    - **Returns**: The value of the query parameter (empty string if not found).
    - **Example**:
      ```cpp
      std::string userId = request.getParam("id");
      ```
      
#### `json getBody();`

- **Description**: Get the body of the request as a JSON object.
- **Returns**: The body of the request as a JSON object.
- **Example**:
  ```cpp
  json body = request.getBody();
  ```

#### `std::string operator[](const std::string &key) const`

- **Description**: Get the value of a header from the request.
- **Parameters**:
  - `key`: The key of the header.
    - **Returns**: The value of the header (empty string if not found).
    - **Example**:
      ```cpp
      std::string contentType = request["Content-Type"];
      ```
  

