# Example API Usage

This document provides examples of how to use the WebServer API to create routes and handle requests. The examples cover common use cases such as defining routes, handling GET and POST requests, and integrating with databases.

## Using static routes

Static routes are routes that serve static content such as HTML files, images, or other assets. You can define static routes using the `get` method of the `Server` class.

```cpp
#include "WebServer/Server.h"

using namespace WebServer;

int main() {
    // Create a new server instance
    WebServer::Server server(8000, 100, 100, 10);

    // Define a route for GET requests to "/login" using path name
    server.get("/login", "../assets/login.html");

    // Define a route for GET requests to "/about" using path name
    server.get("/about", "../assets/about.html");

    // Start the server
    server.start();

    return 0;
}
```

In this example, we create a new `Server` instance and define two static routes for the `/login` and `/about` paths. The server will serve the `login.html` and `about.html` files from the `../assets` directory when these routes are accessed.

## Using function routes

Function routes are routes that execute custom logic when a request is received. You can define function routes using lambda functions or regular functions.

```cpp

#include "WebServer/Server.h"

using namespace WebServer;

int main() {
    // Create a new server instance
    WebServer::Server server(8000, 100, 100, 10);

    // Define a route for POST requests to "/auth" using custom function
    server.post("/auth", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        response.send("Hello World");
    });

    // Define a route for GET requests to "/api/data" using custom function
    server.get("/api/data", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        response.send("Data: {\"key\": \"value\"}");
    });

    // Start the server
    server.start();

    return 0;
}
```

In this example, we define two function routes for the `/auth` and `/api/data` paths. The `/auth` route responds with "Hello World" when a POST request is received, while the `/api/data` route responds with a JSON object when a GET request is received.

## Using dynamic params

Dynamic params allow you to define routes with placeholders that match any value. You can access the values of dynamic params in your route handler.

```cpp
#include "WebServer/Server.h"

using namespace WebServer;

int main() {
    // Create a new server instance
    WebServer::Server server(8000, 100, 100, 10);

    // Define a route with a dynamic param
    server.get("/user/:id", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        std::string userId = request.params["id"];
        response.send("User ID: " + userId);
    });

    // Start the server
    server.start();

    return 0;
}
```

In this example, we define a route with a dynamic param `:id` that matches any value. When a request is received for this route, the route handler extracts the value of the `id` param from the request and sends it back as a response.


