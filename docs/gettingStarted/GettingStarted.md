# Getting Started

This section will guide you through setting up and using WebServer.

## Prerequisites

Before you begin, ensure you have the following prerequisites installed on your system:

- C++ compiler (GCC, Clang, etc.)

## Installation

1. **Clone the repository:**
`git clone [repository_url]`
2. **Include header files**
3. You are ready to go!