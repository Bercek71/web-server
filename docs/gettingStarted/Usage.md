## Usage

Now that you have built the project, follow these steps to start using WebServer:

1. **Integrate WebServer with Your Project:**

   Include the necessary header files from the WebServer framework in your C++ project. You can find these header files in the `include` directory of the WebServer repository.

   ```cpp
   #include "WebServer/Server.h"

   using namespace WebServer;

   int main() {
       // Your server setup code here...
   }

   ```

2. **Define Routes and Server Configuration:** In your C++ project, define routes and configure the server using the WebServer API. Routes can handle various HTTP methods and define the logic to process incoming requests.
    ```cpp
    int main(){

    // Create a new server instance
    WebServer::Server server(8000, 100, 100, 10);

    // Define a route for GET requests to "/login" using path name
    server.get("/login", "../assets/login.html");

    // Define a route for POST requests to "/auth" using custom function
    server.post("/auth", [conn](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        response.send("Hello World");
    });

    // Define more routes...

    // Start the server
    server.start();

    return 0;

    }
    
    ```

3. **Compile Your Project:** 
    Compile your C++ project, ensuring that you link it with the WebServer library. You may need to adjust your build system (e.g., Makefile, CMakeLists.txt) to include WebServer as a dependency.

4. **Run Your Compiled Project:**
    Execute the compiled executable of your project to start the server. Depending on your project's setup, you may need to specify the port number or other configuration options.

5. **Test Your Server:**
   Once your server is running, test it using a web browser or API testing tool. Send requests to the defined routes to verify that the server behaves as expected.

6. **Explore and Customize:**
   Explore the example routes provided in WebServer's documentation and customize them to suit your application's requirements. You can add middleware, error handling, and additional functionality as needed.

Congratulations! You have successfully integrated WebServer into your C++ project and started using it to build web applications. Refer to the project's documentation and examples to explore its features further.
