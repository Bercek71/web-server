# Express like WebServer framework for C++

This library serves as my semestral project for C++ course. Remember this is my university project. It's primary purpose is to learn something new about designing and programming complex projects.

## How to use framework?
This framework is on the same basis as JS Express. It's purpose is to serve as API endpoint for your backend projects.

### Example
```cpp
#include <iostream>
#include <WebServer/WebServer.h>

int main() {
    int port = 8080;
    int threads = 4;
    int timeout = 60;
    int maxConnections = 1024;
    WebServer server(port, threads, maxConnections, timeout);
    server.get("/", [](HttpRequest &req, HttpResponse &res) {
        res.send("Hello World!");
    });
    server.start();
    return 0;
}
```

## How to install

You can install dynamic library using
```bash
cmake -B <build_folder>
cd <build_folder>
make install
```
Feel free to modify CMakeList.txt to set examples for yourself!

## Goals

- [X] Docs
- [X] Better error handling
- [X] Image sending

and more...

> **Note:** You can find more information about this project in [docs](https://web-server-bercek71-54d8bf7319be4eb811c8aa4a6ccbd4b94a9a82c6ff8.gitlab.io/) uploaded on GitLab Pages.
