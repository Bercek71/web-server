#include "../../src/Server.h"
#include <postgresql/libpq-fe.h>
#include "../../lib/json.hpp" // for convenience

using namespace WebServer;

int main() {

    PGconn *conn = PQconnectdb("user=postgres password=pass123 dbname=postgres hostaddr=127.0.0.1 port=1234");

    if (PQstatus(conn) != CONNECTION_OK) {
        Logger::error("Connection to database failed: + " + std::string(PQerrorMessage(conn)));
        PQfinish(conn);
        return 1;
    }

    Logger::log("Connection to database successful");

    WebServer::Server server(8000, 100, 100, 10);


    server.get("/test/:id", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        Logger::debug("Test route GET called!");
        Logger::debug("Request authorization header: " + request["Authorization"]);
        Logger::debug("Request dynamic parameter id: " + request.getParam("id"));
    });

    server.get("/test/:id/:name", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        Logger::debug("Test route GET called!");
        Logger::debug("Request authorization header: " + request["Authorization"]);
        Logger::debug("Request dynamic parameter id: " + request.getParam("id"));
        Logger::debug("Request dynamic parameter name: " + request.getParam("name"));
    });

    server.get("/picture", [](WebServer::HttpRequest &, WebServer::HttpResponse &response) {
        response.sendFile("../assets/img2.png");
    });

    server.get("/login", "../assets/login.html");

    server.post("/auth", [conn](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
                    json requestBody = request.getBody();
                    //                    Logger::debug(requestBody.dump());
                    if (requestBody.find("username") != requestBody.end() && requestBody.find("password") != requestBody.end()) {
                        std::string username = requestBody["username"];
                        std::string password = requestBody["password"];
                        std::string query =
                                "SELECT * FROM users WHERE users.user_name = '" + username + "' AND users.password = '" + password +
                                "'";
                        //                        Logger::debug("Query: " + query);
                        PGresult *res = PQexec(conn, query.c_str());
                        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
                            Logger::error("Query failed: " + std::string(PQerrorMessage(conn)));
                            response.status(500);
                            response.send("Internal Server Error");
                            PQclear(res);
                            return;
                        }
                        if (PQntuples(res) == 0) {
                            response.status(401);
                            response.send("Unauthorized");
                        } else {
                            json responseBody;
                            responseBody["token"] = "Bearer 123456";
                            response.sendJSON(responseBody);

                        }
                        PQclear(res);
                    } else {
                        response.status(400);
                        response.send("Bad Request");
                    }
                }
    );

    server.get("/", "../assets/index.html");
    server.get("/about", "../assets/myPage.html");

    server.post("/test", [](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        Logger::debug("Test route POST called!");
        Logger::debug("Request authorization header: " + request["Authorization"]);
        if (request["authorization"] == "Bearer 123456") {
            Logger::debug("User signed in");
            json j;
            j["message"] = "Signed in";
            response.sendJSON(j);
        } else {
            Logger::debug("User not signed in");
            response.status(401, "Unauthorized");
            json j;
            j["message"] = "Unauthorized";
            response.sendJSON(j);
        }
    });

    server.get("/test2", [](WebServer::HttpRequest &, WebServer::HttpResponse &response) {
        json j;
        j["message"] = "Hello World";
        j["number"] = 42;
        response.sendJSON(j);
    });
    server.get("/style.css", "../assets/style.css");


    server.get("/users", [conn](WebServer::HttpRequest &request, WebServer::HttpResponse &response) {
        if (request["authorization"] != "Bearer 123456") {
            response.status(401, "Unauthorized");
            response.send("Unauthorized");
            return;
        }
        std::string query = "SELECT user_name FROM users";

        PGresult *res = PQexec(conn, query.c_str());
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            Logger::error("Query failed: " + std::string(PQerrorMessage(conn)));
            response.status(500);
            response.send("Internal Server Error");
            PQclear(res);
            return;
        }
        json j;
        for (int i = 0; i < PQntuples(res); i++) {
            j["users"][i] = PQgetvalue(res, i, 0);
        }

        response.sendJSON(j);
    });


//    server.get("/testimage", "../eye_closed_template.png");

    server.start();
    return 0;
}
