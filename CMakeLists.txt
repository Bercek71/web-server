cmake_minimum_required(VERSION 3.22.1)
project(Projekt)

set(CMAKE_CXX_STANDARD 17)

# Find libpq
find_package(PkgConfig REQUIRED)
pkg_check_modules(LIBPQ REQUIRED libpq)

# Include directories for libpq
include_directories(${LIBPQ_INCLUDE_DIRS})

#create library

add_library(WebServer src/Server.cpp
        src/Server.h
        src/Logger.cpp
        src/Logger.h
        src/HttpHeader.cpp
        src/HttpHeader.h
        src/Route.cpp
        src/Route.h
        src/RouteBuilder.cpp
        src/RouteBuilder.h
        src/HttpRequest.cpp
        src/HttpRequest.h
        src/HttpResponse.cpp
        src/HttpResponse.h
        src/ContentType.cpp
        src/ContentType.h
        src/Path.cpp
        src/Path.h
)

add_executable(Projekt examples/DatabaseExample/main.cpp)


target_link_libraries(Projekt ${LIBPQ_LIBRARIES} WebServer)
