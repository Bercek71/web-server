//
// Created by nyansapo on 28.2.24.
//

#ifndef PROJEKT_HTTPHEADER_H
#define PROJEKT_HTTPHEADER_H

#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "Logger.h"

namespace WebServer {
class HttpHeader {
  std::map<std::string, std::string> headers;
  bool validateHeader();

 public:
  HttpHeader() = default;
  explicit HttpHeader(const std::map<std::string, std::string> &headers);
  void parseHeader(const std::string &headerString);

  std::string getHeader(const std::string &headerName);
  bool hasHeader(const std::string &headerName);
  std::vector<std::string> getHeaderNames();

  void setHeader(const std::string &headerName, const std::string &headerValue);
  std::string operator[](const std::string &headerName);

  //    void removeHeader(const std::string &headerName);

  std::string toString();
};

}  // namespace WebServer

#endif  // PROJEKT_HTTPHEADER_H
