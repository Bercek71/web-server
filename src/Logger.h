//
// Created by nyansapo on 27.2.24.
//

#ifndef PROJEKT_LOGGER_H
#define PROJEKT_LOGGER_H

#include <cstdarg>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

namespace WebServer {

class Logger {
 private:
  static void appendToFile(const string &message);

  static string getTime();

  static string logPath;
  static bool shouldLogToFile;

 public:
  static void log(const string &message);

  static void warn(const string &message);

  static void error(const string &message);

  static void debug(const string &message);

  static void setLogPath(const string &path);

  static void setShouldLogToFile(bool shouldLog);
};

}  // namespace WebServer

#endif  // PROJEKT_LOGGER_H
