//
// Created by nyansapo on 29.2.24.
//

#ifndef PROJEKT_ROUTE_H
#define PROJEKT_ROUTE_H

#include <functional>
#include <optional>
#include <string>
#include <utility>

#include "HttpRequest.h"
#include "HttpResponse.h"
#include "Path.h"

namespace WebServer {

using RequestHandler = std::function<void(HttpRequest &, HttpResponse &)>;

class Route {
  std::string method;
  std::optional<std::string> fileName;
  std::optional<RequestHandler> handler;
  Path path;
 public:
  Route(std::string path, std::string method,
        std::optional<std::string> fileName,
        std::optional<RequestHandler> handler);

  [[nodiscard]] Path getPath() const;

  [[nodiscard]] std::string getMethod() const;

  [[nodiscard]] RequestHandler getHandler() const;

  [[nodiscard]] std::string getFileName() const;

  [[nodiscard]] bool hasFileName() const;

  [[nodiscard]] bool hasHandler() const;

  bool operator==(const Route &other) const;
};
}  // namespace WebServer

#endif  // PROJEKT_ROUTE_H
