//
// Created by bercek on 17.3.24.
//

#ifndef PROJEKT_CONTENTTYPE_H
#define PROJEKT_CONTENTTYPE_H

#include <string>

namespace WebServer {
enum class ContentType {
  TEXT_PLAIN,
  TEXT_HTML,
  TEXT_CSS,
  TEXT_JAVASCRIPT,
  APPLICATION_JSON,
  APPLICATION_XML,
  APPLICATION_ZIP,
  APPLICATION_PDF,
  APPLICATION_X_WWW_FORM_URLENCODED,
  IMAGE_JPEG,
  IMAGE_PNG,
  IMAGE_GIF,
  IMAGE_BMP,
  IMAGE_SVG,
  IMAGE_WEBP,
  AUDIO_MPEG,
  AUDIO_OGG,
  AUDIO_WAV,
  AUDIO_FLAC,
  AUDIO_AAC,
  AUDIO_WEBM,
  VIDEO_MPEG,
  VIDEO_OGG,
  VIDEO_WEBM,
  VIDEO_MP4,
  VIDEO_AVI,
  VIDEO_FLV,
  VIDEO_MOV,
  VIDEO_3GP,
  None
};
std::string contentTypeToString(ContentType contentType);
ContentType stringToContentType(const std::string &contentType);
ContentType fileExtensionToContentType(const std::string &fileName);
}  // namespace WebServer

#endif  // PROJEKT_CONTENTTYPE_H
