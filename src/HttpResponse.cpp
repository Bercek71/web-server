//
// Created by bercek on 17.3.24.
//

#include "HttpResponse.h"

#include <utility>

namespace WebServer {
    void HttpResponse::send(std::string body) {
        if (locked) {
            return;
        }
        this->body = std::move(body);
        if (this->contentType == ContentType::None) {
            this->contentType = ContentType::TEXT_HTML;
        }
        if (this->statusCode == -1) {
            this->statusCode = 200;
            this->statusMessage = "OK";
        }
        locked = true;
        prepareHeaders();
    }

    void HttpResponse::prepareHeaders() {
        if (!locked) {
            return;
        }
        headers.setHeader("Content-Length", std::to_string(body.size()));
        headers.setHeader("Content-Type", contentTypeToString(contentType));
        headers.setHeader("Connection", "close");
        headers.setHeader("Server", "WebServer");
    }

    HttpResponse HttpResponse::status(int statusCode, std::string statusMessage) {
        if (locked) return *this;
        this->statusCode = statusCode;
        this->statusMessage = std::move(statusMessage);
        return *this;
    }

    HttpResponse HttpResponse::status(int statusCode) {
        if (locked) return *this;
        this->statusCode = statusCode;
        if (statusCode == 200)
            this->statusMessage = "OK";
        else if (statusCode == 404)
            this->statusMessage = "Not Found";
        else if (statusCode == 500)
            this->statusMessage = "Internal Server Error";
        else if (statusCode == 400)
            this->statusMessage = "Bad Request";
        else if (statusCode == 401)
            this->statusMessage = "Unauthorized";
        else if (statusCode == 403)
            this->statusMessage = "Forbidden";
        else if (statusCode == 405)
            this->statusMessage = "Method Not Allowed";
        else if (statusCode == 301)
            this->statusMessage = "Moved Permanently";
        else if (statusCode == 302)
            this->statusMessage = "Found";
        else if (statusCode == 303)
            this->statusMessage = "See Other";
        else if (statusCode == 304)
            this->statusMessage = "Not Modified";
        else if (statusCode == 307)
            this->statusMessage = "Temporary Redirect";
        else if (statusCode == 308)
            this->statusMessage = "Permanent Redirect";
        else
            this->statusMessage = "OK";

        return *this;
    }

    void HttpResponse::sendJSON(json &body) {
        if (locked) {
            return;
        }
        this->body = std::move(to_string(body));
        if (this->contentType == ContentType::None) {
            this->contentType = ContentType::APPLICATION_JSON;
        }
        if (this->statusCode == -1) {
            this->statusCode = 200;
            this->statusMessage = "OK";
        }
        locked = true;
        prepareHeaders();
    }

    int HttpResponse::getStatusCode() const { return statusCode; }

    std::string HttpResponse::getStatusMessage() const { return statusMessage; }

    std::string HttpResponse::getBody() const { return body; }

    HttpHeader HttpResponse::getHeaders() const { return headers; }

    std::string HttpResponse::toString() {
        std::string response;
        response =
                "HTTP/1.1 " + std::to_string(statusCode) + " " + statusMessage + "\r\n";
        response.append(headers.toString());
        response.append("\r\n");
        response.append(body);
        //        Logger::debug(response);
        return response;
    }

    bool HttpResponse::isLocked() const { return locked; }

    void HttpResponse::setContentType(ContentType contentType) {
        this->contentType = contentType;
    }

    void HttpResponse::sendFile(std::string path) {
        if (locked) {
            return;
        }

        std::ifstream file(path, std::ios::binary | std::ios::ate);

        if (!file.is_open()) {
            this->statusCode = 404;
            this->statusMessage = "Not Found";
            this->body = "404 Not Found";
            locked = true;
            prepareHeaders();
            return;
        }

        std::streamsize fileSize = file.tellg();
        file.seekg(0, std::ios::beg);
        this->body = std::string(fileSize, 0);
        if (!file.read(&body[0], fileSize)) {
            std::cerr << "Error reading image file\n";
            return;
        }
        file.close();


        contentType = convertFileMimeType(path);

        if (this->statusCode == -1) {
            this->statusCode = 200;
            this->statusMessage = "OK";
        }

        locked = true;

        prepareHeaders();

    }

    ContentType HttpResponse::convertFileMimeType(string &path) {
        string extension = path.substr(path.find_last_of('.') + 1);

        if (extension == "html" || extension == "htm") {
            return ContentType::TEXT_HTML;
        } else if (extension == "css") {
            return ContentType::TEXT_CSS;
        } else if (extension == "json") {
            return ContentType::APPLICATION_JSON;
        } else if (extension == "xml") {
            return ContentType::APPLICATION_XML;
        } else if (extension == "jpeg" || extension == "jpg") {
            return ContentType::IMAGE_JPEG;
        } else if (extension == "png") {
            return ContentType::IMAGE_PNG;
        } else if (extension == "gif") {
            return ContentType::IMAGE_GIF;
        } else if (extension == "bmp") {
            return ContentType::IMAGE_BMP;
        } else if (extension == "mp3") {
            return ContentType::AUDIO_MPEG;
        } else if (extension == "mp4") {
            return ContentType::VIDEO_MP4;
        } else if (extension == "mpeg") {
            return ContentType::VIDEO_MPEG;
        } else if (extension == "webm") {
            return ContentType::VIDEO_WEBM;
        } else if (extension == "pdf") {
            return ContentType::APPLICATION_PDF;
        } else if (extension == "zip") {
            return ContentType::APPLICATION_ZIP;
        } else {
            return ContentType::None;
        }
    }


}  // namespace WebServer