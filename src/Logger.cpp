//
// Created by nyansapo on 27.2.24.
//

#include "Logger.h"

namespace WebServer {

string Logger::getTime() {
  const std::time_t now = std::time(nullptr);
  const std::tm *localTime =
      std::localtime(&now);                    // Convert time to local time
  const int hours = localTime->tm_hour;        // Get the current hour
  const int minutes = localTime->tm_min;       // Get the current minute
  const int day = localTime->tm_mday;          // Get the current day
  const int month = localTime->tm_mon + 1;     // Get the current month
  const int year = localTime->tm_year + 1900;  // Get the current year
  string hoursString{};
  string minutesString{};
  string dayString{};
  string monthString{};
  string yearString{};
  if (hours < 10) {
    hoursString = "0" + std::to_string(hours);
  } else {
    hoursString = std::to_string(hours);
  }
  if (minutes < 10) {
    minutesString = "0" + std::to_string(minutes);
  } else {
    minutesString = std::to_string(minutes);
  }
  if (day < 10) {
    dayString = "0" + std::to_string(day);
  } else {
    dayString = std::to_string(day);
  }
  if (month < 10) {
    monthString = "0" + std::to_string(month);
  } else {
    monthString = std::to_string(month);
  }
  yearString = std::to_string(year);
  return dayString + "." + monthString + ". " + yearString + " " + hoursString +
         ":" + minutesString;
}

void Logger::debug(const string &message) {
  std::string logString = "[ " + getTime() + " ] [ DEBUG ] " + message;
  appendToFile(logString);
  std::cout << "\033[1;32m" << logString << "\033[0m" << std::endl;
}

void Logger::error(const string &message) {
  std::string logString = "[ " + getTime() + " ] [ ERROR ] " + message;
  appendToFile(logString);
  std::cout << "\033[1;31m" << logString << "\033[0m" << std::endl;
}

void Logger::warn(const string &message) {
  std::string logString = "[ " + getTime() + " ] [ WARNING ] " + message;
  appendToFile(logString);
  std::cout << "\033[1;33m" << logString << "\033[0m" << std::endl;
}

void Logger::log(const string &message) {
  std::string logString = "[ " + getTime() + " ] [ LOG ] " + message;
  appendToFile(logString);
  std::cout << logString << std::endl;
}

void Logger::appendToFile(const string &message) {
  if (!shouldLogToFile) {
    return;
  }
  std::ofstream file;
  file.open(logPath, std::ios::app);
  file << message << std::endl;
  file.close();
}

void Logger::setLogPath(const string &path) { logPath = path; }

void Logger::setShouldLogToFile(bool shouldLog) { shouldLogToFile = shouldLog; }

string Logger::logPath = "log.txt";

bool Logger::shouldLogToFile = true;

}  // namespace WebServer
