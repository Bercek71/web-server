
#include <stdexcept>
#include "Path.h"


Path::Path(std::string path) : path(std::move(path)), dynamicIndexes(std::vector<size_t>()),
                               parts(std::vector<std::string>()) {

    //find '/' and split path
    //if there is a ':' in the begining of part, add it to dynamicIndexes
    size_t start = 0;
    size_t end = 0;
    while (end != std::string::npos) {
        end = this->path.find('/', start);
        std::string part = this->path.substr(start, end - start);
        if (part[0] == ':') {
            dynamicIndexes.push_back(parts.size());
        }
        parts.push_back(part);
        start = end + 1;
    }
}

bool Path::operator==(const std::string &other) const {
    size_t start = 0;
    size_t end = 0;
    std::vector<std::string> otherParts;
    while (end != std::string::npos) {
        end = other.find('/', start);
        std::string part = other.substr(start, end - start);
        otherParts.push_back(part);
        start = end + 1;
    }
    if (otherParts.size() != parts.size()) {
        return false;
    }

    for (size_t i = 0; i < parts.size(); i++) {
        if (parts[i][0] == ':') {
            continue;
        }
        if (parts[i] != otherParts[i]) {
            return false;
        }
    }

    return true;
}

std::map<std::string, std::string> Path::getParams(const std::string &other) const {
    size_t start = 0;
    size_t end = 0;
    std::vector<std::string> otherParts;
    while (end != std::string::npos) {
        end = other.find('/', start);
        std::string part = other.substr(start, end - start);
        otherParts.push_back(part);
        start = end + 1;
    }
    if (otherParts.size() != parts.size()) {
        throw std::invalid_argument("Path does not match");
    }

    std::map<std::string, std::string> params;
    for (size_t i = 0; i < parts.size(); i++) {
        if (parts[i][0] == ':') {
            params[parts[i].substr(1)] = otherParts[i];
        }
    }

    return params;
}

bool Path::operator==(const Path &other) const {
    if (parts.size() != other.parts.size()) {
        return false;
    }

    for (size_t i = 0; i < parts.size(); i++) {
        if (parts[i][0] == ':' && other.parts[i][0] == ':') {
            continue;
        }
        if (parts[i] != other.parts[i]) {
            return false;
        }
    }

    return true;
}

bool Path::operator!=(const Path &other) const {
    return !(*this == other);
}

std::vector<size_t> Path::getDynamicIndexes() {
    return dynamicIndexes;
}

std::string Path::operator[](size_t index) {
    if (index >= parts.size()) {
        throw std::out_of_range("Index out of range");
    }
    return parts[index];
}

std::string Path::getRawPath() {
    return path;
}

std::vector<std::string> Path::getDynamicParamsNames() {
    std::vector<std::string> names;
    for (size_t i : dynamicIndexes) {
        names.push_back(parts[i].substr(1));
    }
    return names;
}

bool Path::hasDynamicParams() {
    return !dynamicIndexes.empty();
}
