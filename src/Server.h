#ifndef PROJEKT_SERVER_H
#define PROJEKT_SERVER_H

#include <arpa/inet.h>
#include <netinet/in.h>

#include <atomic>
#include <csignal>
#include <thread>
#include <vector>

#include "HttpHeader.h"
#include "HttpRequest.h"
#include "HttpResponse.h"
#include "Logger.h"
#include "Route.h"
#include "RouteBuilder.h"

namespace WebServer {

class Server {
 private:
  int serverSocket;
  int port;
  int maxConnections;
  int maxThreads;
  int timeout;
  std::thread connectionListener;
  std::vector<Route> routes;
  static std::atomic<bool> running;

  static void signalHandler(int signal);

  void handleClient(int clientSocket);

  std::string readFile(const std::string &fileName);
  std::string readFileBinary(const std::string &fileName);

  void addRoute(Route &route);

  void addRoute(Route &&route);

  bool routeExists(Route &route);

  RequestHandler createSendFileHandler(const std::string &path,
                                       const std::string &fileName,
                                       const std::string &method);

 public:
  Server(int port, int maxConnections, int maxThreads, int timeout);

  void get(const std::string &path, const std::string &fileName);

  void get(const std::string &path, RequestHandler &&handler);

  void post(const std::string &path, const std::string &fileName);

  void post(const std::string &path, RequestHandler &&handler);

  void put(const std::string &path, const std::string &fileName);

  void put(const std::string &path, RequestHandler &&handler);

  void del(const std::string &path, const std::string &fileName);

  void del(const std::string &path, RequestHandler &&handler);

  [[noreturn]] void start();
};

}  // namespace WebServer

#endif  // PROJEKT_SERVER_H
