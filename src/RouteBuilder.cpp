//
// Created by bercek on 16.3.24.
//

#include "RouteBuilder.h"

namespace WebServer {
RouteBuilder::RouteBuilder(std::string method, std::string path)
    : method(std::move(method)), path(std::move(path)) {}

RouteBuilder &RouteBuilder::setFileName(const string &fileName) {
  this->fileName = std::optional<std::string>(fileName);
  return *this;
}

RouteBuilder &RouteBuilder::setHandler(
    std::function<void(HttpRequest &, HttpResponse &)> handler) {
  this->handler = std::optional<RequestHandler>(handler);
  return *this;
}

Route RouteBuilder::build() { return {path, method, fileName, handler}; }
}  // namespace WebServer