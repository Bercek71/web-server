#ifndef PROJEKT_PATH_H
#define PROJEKT_PATH_H


#include <string>
#include <vector>
#include <utility>
#include <map>


class Path {
private:
    std::string path;
    std::vector<std::string> parts;
    std::vector<size_t> dynamicIndexes;
public:
    explicit Path(std::string path);

    bool operator==(const Path &other) const;

    bool operator==(const std::string &other) const;

    [[nodiscard]] std::map<std::string, std::string> getParams(const std::string &other) const;

    bool operator!=(const Path &other) const;

    std::vector<std::string> getDynamicParamsNames();

    bool hasDynamicParams();

    std::vector<size_t> getDynamicIndexes();

    std::string operator[](size_t index);

    std::string getRawPath();
};


#endif //PROJEKT_PATH_H
