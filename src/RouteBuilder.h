//
// Created by bercek on 16.3.24.
//

#ifndef PROJEKT_ROUTEBUILDER_H
#define PROJEKT_ROUTEBUILDER_H

#include <functional>
#include <optional>
#include <string>

#include "HttpHeader.h"
#include "Route.h"
namespace WebServer {

class RouteBuilder {
 private:
  std::optional<std::string> fileName;
  std::optional<RequestHandler> handler;
  std::string path;
  std::string method;

 public:
  RouteBuilder(std::string method, std::string path);

  RouteBuilder &setFileName(const std::string &fileName);

  RouteBuilder &setHandler(
      std::function<void(HttpRequest &, HttpResponse &)> handler);

  Route build();
};

}  // namespace WebServer

#endif  // PROJEKT_ROUTEBUILDER_H
