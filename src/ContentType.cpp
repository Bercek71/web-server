//
// Created by bercek on 17.3.24.
//

#include "ContentType.h"

std::string WebServer::contentTypeToString(WebServer::ContentType contentType) {
  switch (contentType) {
    case ContentType::TEXT_PLAIN:
      return "text/plain";
    case ContentType::TEXT_HTML:
      return "text/html";
    case ContentType::TEXT_CSS:
      return "text/css";
    case ContentType::TEXT_JAVASCRIPT:
      return "text/javascript";
    case ContentType::APPLICATION_JSON:
      return "application/json";
    case ContentType::APPLICATION_XML:
      return "application/xml";
    case ContentType::APPLICATION_ZIP:
      return "application/zip";
    case ContentType::APPLICATION_PDF:
      return "application/pdf";
    case ContentType::IMAGE_JPEG:
      return "image/jpeg";
    case ContentType::IMAGE_PNG:
      return "image/png";
    case ContentType::IMAGE_GIF:
      return "image/gif";
    case ContentType::IMAGE_BMP:
      return "image/bmp";
    case ContentType::IMAGE_SVG:
      return "image/svg+xml";
    case ContentType::IMAGE_WEBP:
      return "image/webp";
    case ContentType::AUDIO_MPEG:
      return "audio/mpeg";
    case ContentType::AUDIO_OGG:
      return "audio/ogg";
    case ContentType::AUDIO_WAV:
      return "audio/wav";
    case ContentType::AUDIO_FLAC:
      return "audio/flac";
    case ContentType::AUDIO_AAC:
      return "audio/aac";
    case ContentType::AUDIO_WEBM:
      return "audio/webm";
    case ContentType::VIDEO_MPEG:
      return "video/mpeg";
    case ContentType::VIDEO_OGG:
      return "video/ogg";
    case ContentType::VIDEO_WEBM:
      return "video/webm";
    case ContentType::VIDEO_MP4:
      return "video/mp4";
    case ContentType::VIDEO_AVI:
      return "video/avi";
    case ContentType::VIDEO_FLV:
      return "video/flv";
    case ContentType::VIDEO_MOV:
      return "video/quicktime";
    case ContentType::VIDEO_3GP:
      return "video/3gpp";
    case ContentType::APPLICATION_X_WWW_FORM_URLENCODED:
      return "application/x-www-form-urlencoded";
    default:
      return "text/plain";
  }
}

WebServer::ContentType WebServer::stringToContentType(
    const std::string &contentTypeString) {
  std::string contentType = contentTypeString;
  contentType = contentType.substr(0, contentType.find('\r'));
  if (contentType == "text/plain") {
    return ContentType::TEXT_PLAIN;
  } else if (contentType == "text/html") {
    return ContentType::TEXT_HTML;
  } else if (contentType == "text/css") {
    return ContentType::TEXT_CSS;
  } else if (contentType == "text/javascript") {
    return ContentType::TEXT_JAVASCRIPT;
  } else if (contentType == "application/json") {
    return ContentType::APPLICATION_JSON;
  } else if (contentType == "application/xml") {
    return ContentType::APPLICATION_XML;
  } else if (contentType == "application/zip") {
    return ContentType::APPLICATION_ZIP;
  } else if (contentType == "application/pdf") {
    return ContentType::APPLICATION_PDF;
  } else if (contentType == "image/jpeg") {
    return ContentType::IMAGE_JPEG;
  } else if (contentType == "image/png") {
    return ContentType::IMAGE_PNG;
  } else if (contentType == "image/gif") {
    return ContentType::IMAGE_GIF;
  } else if (contentType == "image/bmp") {
    return ContentType::IMAGE_BMP;
  } else if (contentType == "image/svg+xml") {
    return ContentType::IMAGE_SVG;
  } else if (contentType == "image/webp") {
    return ContentType::IMAGE_WEBP;
  } else if (contentType == "audio/mpeg") {
    return ContentType::AUDIO_MPEG;
  } else if (contentType == "audio/ogg") {
    return ContentType::AUDIO_OGG;
  } else if (contentType == "audio/wav") {
    return ContentType::AUDIO_WAV;
  } else if (contentType == "audio/flac") {
    return ContentType::AUDIO_FLAC;
  } else if (contentType == "audio/aac") {
    return ContentType::AUDIO_AAC;
  } else if (contentType == "audio/webm") {
    return ContentType::AUDIO_WEBM;
  } else if (contentType == "video/mpeg") {
    return ContentType::VIDEO_MPEG;
  } else if (contentType == "video/ogg") {
    return ContentType::VIDEO_OGG;
  } else if (contentType == "video/webm") {
    return ContentType::VIDEO_WEBM;
  } else if (contentType == "application/x-www-form-urlencoded") {
    return ContentType::APPLICATION_X_WWW_FORM_URLENCODED;
  } else {
    return ContentType::None;
  }
}

WebServer::ContentType WebServer::fileExtensionToContentType(
    const std::string &fileName) {
  ContentType contentType;
  if (fileName.find(".html") != std::string::npos) {
    contentType = ContentType::TEXT_HTML;
  } else if (fileName.find(".css") != std::string::npos) {
    contentType = ContentType::TEXT_CSS;
  } else if (fileName.find(".js") != std::string::npos) {
    contentType = ContentType::TEXT_JAVASCRIPT;
  } else if (fileName.find(".json") != std::string::npos) {
    contentType = ContentType::APPLICATION_JSON;
  } else if (fileName.find(".jpg") != std::string::npos) {
    contentType = ContentType::IMAGE_JPEG;
  } else if (fileName.find(".png") != std::string::npos) {
    contentType = ContentType::IMAGE_PNG;
  } else if (fileName.find(".gif") != std::string::npos) {
    contentType = ContentType::IMAGE_GIF;
  } else {
    contentType = ContentType::TEXT_PLAIN;
  }
  return contentType;
}
