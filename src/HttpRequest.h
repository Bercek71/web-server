#ifndef PROJEKT_HTTPREQUEST_H
#define PROJEKT_HTTPREQUEST_H

#include <algorithm>
#include <map>
#include <optional>
#include <sstream>
#include <string>

#include "../lib/json.hpp"
#include "ContentType.h"
#include "HttpHeader.h"

using namespace nlohmann;
namespace WebServer {

class HttpRequest {
 private:
  static HttpRequest parseRequest(const std::string &requestString);
  std::optional<ContentType> contentType;
  json body;
  std::string method;
  std::string path;
  std::string protocol;
  bool empty = false;
  HttpHeader headers;
  std::map<std::string, std::string> dynamicParams;

 public:
  HttpRequest(std::string method, std::string path, std::string protocol,
              HttpHeader headers, json body);
  explicit HttpRequest(const std::string &rawRequest);

  std::string operator[](const std::string &key);

  std::string getMethod();

  std::string getPath();

  std::string getProtocol();

  std::string getParam(const std::string &paramName);

  void setParams(const std::map<std::string, std::string> &params);

  json getBody();

  [[nodiscard]] bool isEmpty() const;

  bool hasHeader(const std::string &headerName);
};

}  // namespace WebServer
#endif  // PROJEKT_HTTPREQUEST_H
