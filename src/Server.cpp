#include "Server.h"

#include <cstring>

namespace WebServer {

    std::atomic<bool> Server::running(false);

    Server::Server(int port, int maxConnections, int maxThreads, int timeout)
            : port(port),
              maxConnections(maxConnections),
              maxThreads(maxThreads),
              timeout(timeout) {
        if (Server::running.load()) {
            Logger::error("Server is already running!");
            exit(1);
        }

        serverSocket = socket(AF_INET, SOCK_STREAM, 0);
        if (serverSocket == -1) {
            Logger::error("Failed to create server socket!");
            exit(1);
        }
        sockaddr_in serverAddress{};
        serverAddress.sin_family = AF_INET;
        serverAddress.sin_port = htons(port);
        serverAddress.sin_addr.s_addr = INADDR_ANY;
        if (bind(serverSocket, (sockaddr *) &serverAddress, sizeof(serverAddress)) ==
            -1) {
            Logger::error("Failed to bind server socket!");
            exit(1);
        }
        if (listen(serverSocket, maxConnections) == -1) {
            Logger::error("Failed to listen on server socket!");
            exit(1);
        }
        routes = std::vector<Route>();
    }

    [[noreturn]] void Server::start() {
        if (Server::running.load()) {
            Logger::error("Server is already running!");
            exit(1);
        }
        running.store(true);
        signal(SIGINT, reinterpret_cast<__sighandler_t>(&Server::signalHandler));
        Logger::log("Server started on port " + std::to_string(port));
        // create thread to listen for connections
        while (Server::running.load()) {
            sockaddr_in clientAddress{};
            socklen_t clientAddressSize = sizeof(clientAddress);
            int clientSocket =
                    accept(serverSocket, (sockaddr *) &clientAddress, &clientAddressSize);
            if (clientSocket == -1) {
                Logger::error("Failed to accept client connection!");
                continue;
            }
            Logger::log("Accepted client connection from " +
                        std::string(inet_ntoa(clientAddress.sin_addr)));
            // create thread to handle client
            std::thread clientHandler(&Server::handleClient, this, clientSocket);
            clientHandler.detach();
        }
        close(serverSocket);
        exit(0);
    }

    void Server::handleClient(int clientSocket) {
        char buffer[1024];
        size_t bytesRead;
        std::string headerString{};
        // Set timeout
        timeval tv{};
        tv.tv_sec = timeout;
        tv.tv_usec = 0;
        try {
            setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, (const char *) &tv,
                       sizeof tv);
            while ((bytesRead = recv(clientSocket, buffer, sizeof(buffer), 0)) > 0) {
                Logger::log("Received " + std::to_string(bytesRead) +
                            " bytes from client");
                headerString.append(buffer, bytesRead);
                if (bytesRead <= 1024) {
                    break;
                }
            }
            if (bytesRead == -1) {
                Logger::error("Failed to receive data from client!");
                close(clientSocket);
                return;
            }
            if (bytesRead == 0) {
                Logger::log("Client closed connection!");
                return;
            }

            HttpRequest request(headerString);

            if (request.isEmpty()) {
                Logger::warn("Received empty request!");
                close(clientSocket);
                return;
            }

            HttpResponse httpResponse;
            std::string body{};
            for (const auto &route: routes) {
                if (route.getMethod() == request.getMethod() &&
                    route.getPath() == request.getPath()) {
                    if (route.getPath().hasDynamicParams()) {
                        auto params = route.getPath().getParams(request.getPath());
                        request.setParams(params);
                    }
                    if (route.hasHandler()) {
                        try {
                            route.getHandler()(request, httpResponse);
                            break;
                        } catch (std::exception &e) {
                            std::string error(e.what());
                            Logger::error("Failed to handle request: " + error);
                            httpResponse.status(500);
                            httpResponse.send("Internal Server Error");
                            break;
                        }
                    }
                }
            }

            if (!httpResponse.isLocked()) {
                body = readFile("../notFound.html");
                httpResponse.status(404);
                httpResponse.send(body);
            }

            std::string strResponse = httpResponse.toString();
            send(clientSocket, strResponse.data(), strResponse.size(), 0);
            close(clientSocket);
        } catch (std::exception &e) {
            Logger::warn("Connection timeout");
            close(clientSocket);
        }
    }

    void Server::addRoute(Route &route) { routes.push_back(route); }

    std::string Server::readFile(const string &fileName) {
        try {
            std::ifstream file(fileName);
            std::string fileContent;
            char buffer[1024];
            while (!file.eof()) {
                file.read(buffer, sizeof(buffer));
                fileContent.append(buffer, file.gcount());
            }
            return fileContent;
        } catch (std::exception &e) {
            Logger::error("Failed to read file " + fileName + ": " + e.what());
            return "";
        }
    }

    void Server::signalHandler(int signal) {
        Logger::log("Received signal " + std::to_string(signal));
        Server::running.store(false);
        exit(0);
    }

    void Server::addRoute(Route &&route) { routes.push_back(route); }

    void Server::get(const string &path, const string &fileName) {
        RouteBuilder builder("GET", path);
        //        builder.setFileName(fileName);
        builder.setHandler(createSendFileHandler(path, fileName, "GET"));
        auto route = builder.build();
        if (routeExists(route)) {
            //            Logger::warn("Route GET " + path + " already exists!");
            throw std::runtime_error("Route GET " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::get(const string &path, RequestHandler &&handler) {
        RouteBuilder builder("GET", path);
        builder.setHandler(handler);
        auto route = builder.build();
        if (routeExists(route)) {
            //            Logger::warn("Route GET " + path + " already exists!");
            throw std::runtime_error("Route GET " + path + " already exists!");
        }
        addRoute(route);
    }

    bool Server::routeExists(Route &route) {
        for (const auto &r: routes) {
            if (r == route) return true;
        }
        return false;
    }

    void Server::post(const string &path, const string &fileName) {
        RouteBuilder builder("POST", path);
        builder.setFileName(fileName);
        auto route = builder.build();
        if (routeExists(route)) {
            throw std::runtime_error("Route POST " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::post(const string &path, RequestHandler &&handler) {
        RouteBuilder builder("POST", path);
        builder.setHandler(handler);
        auto route = builder.build();
        if (routeExists(route)) {
            throw std::runtime_error("Route POST " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::put(const string &path, const string &fileName) {
        RouteBuilder builder("PUT", path);
        builder.setFileName(fileName);
        auto route = builder.build();
        if (routeExists(route)) {
            //            Logger::warn("Route PUT " + path + " already exists!");
            throw std::runtime_error("Route PUT " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::put(const string &path, RequestHandler &&handler) {
        RouteBuilder builder("PUT", path);
        builder.setHandler(handler);
        auto route = builder.build();
        if (routeExists(route)) {
            throw std::runtime_error("Route PUT " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::del(const string &path, const string &fileName) {
        RouteBuilder builder("DELETE", path);
        builder.setFileName(fileName);
        auto route = builder.build();
        if (routeExists(route)) {
            throw std::runtime_error("Route DELETE " + path + " already exists!");
        }
        addRoute(route);
    }

    void Server::del(const string &path, RequestHandler &&handler) {
        RouteBuilder builder("DELETE", path);
        builder.setHandler(handler);
        auto route = builder.build();
        if (routeExists(route)) {
            throw std::runtime_error("Route DELETE " + path + " already exists!");
        }
        addRoute(route);
    }

    RequestHandler Server::createSendFileHandler(const string &path,
                                                 const string &fileName,
                                                 const string &method) {
        ContentType contentType = fileExtensionToContentType(fileName);

        std::string body = Server::readFile(fileName);

        //        TODO handle binary files

        return
                [path, body, contentType](HttpRequest &request, HttpResponse &response) {
                    response.setContentType(contentType);
                    response.send(body);
                };
    }

    std::string Server::readFileBinary(const string &fileName) {
        try {
            std::ifstream file(fileName, std::ios::binary);
            std::string fileContent;
            char buffer[1024];
            while (!file.eof()) {
                file.read(buffer, sizeof(buffer));
                fileContent.append(buffer, file.gcount());
            }
            return fileContent;
        } catch (std::exception &e) {
            Logger::error("Failed to read file " + fileName + ": " + e.what());
            return "";
        }
    }

}  // namespace WebServer