#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <atomic>
#include <stdexcept>
#include <semaphore.h>

class ThreadPool {
public:
    ThreadPool(size_t numThreads, size_t maxTasks) : stop(false), maxTasks(maxTasks), numTasks(0) {
        sem_init(&taskSemaphore, 0, maxTasks);
        for (size_t i = 0; i < numThreads; ++i) {
            workers.emplace_back([this] {
                while (true) {
                    std::function<void()> task;
                    {
                        std::unique_lock<std::mutex> lock(this->queue_mutex);
                        this->condition.wait(lock, [this] {
                            return this->stop || !this->tasks.empty();
                        });
                        if (this->stop && this->tasks.empty()) {
                            return;
                        }
                        task = std::move(this->tasks.front());
                        this->tasks.pop();
                    }
                    task();
                    sem_post(&taskSemaphore);
                }
            });
        }
    }

    template<class F, class... Args>
    void addTask(F&& f, Args&&... args) {
        sem_wait(&taskSemaphore);
        {
            std::unique_lock<std::mutex> lock(queue_mutex);

            if (stop) {
                throw std::runtime_error("addTask on stopped ThreadPool");
            }

            tasks.emplace([f = std::forward<F>(f), args = std::make_tuple(std::forward<Args>(args)...)]() {
                std::apply(f, args);
            });
        }
        condition.notify_one();
    }

    ~ThreadPool() {
        {
            std::unique_lock<std::mutex> lock(queue_mutex);
            stop = true;
        }
        condition.notify_all();
        for (std::thread &worker : workers) {
            worker.join();
        }
        sem_destroy(&taskSemaphore);
    }

private:
    std::vector<std::thread> workers;
    std::queue<std::function<void()>> tasks;

    std::mutex queue_mutex;
    std::condition_variable condition;
    std::atomic<bool> stop;
    size_t maxTasks;
    size_t numTasks;
    sem_t taskSemaphore;
};
