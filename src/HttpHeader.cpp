//
// Created by nyansapo on 28.2.24.
//

#include "HttpHeader.h"

#include <utility>

std::string WebServer::HttpHeader::getHeader(
    const std::string &headerName) {
  return headers[headerName];
}

std::string WebServer::HttpHeader::toString() {
  std::string headerString;
  for (auto &header : headers) {
    headerString += header.first + ": " + header.second + "\r\n";
  }
  return headerString;
}

void WebServer::HttpHeader::parseHeader(const std::string &headerString) {
  std::istringstream iss(headerString);
  std::string line;

  // Parsing request line
  std::getline(iss, line);
  std::istringstream reqLine(line);
  std::string method, path, protocol;
  reqLine >> method >> path >> protocol;

  headers["method"] = method;
  headers["path"] = path;
  headers["protocol"] = protocol;

  // Parsing other headers
  while (std::getline(iss, line)) {
    if (line.empty()) continue;
    size_t pos = line.find(": ");
    if (pos != std::string::npos) {
      std::string key = line.substr(0, pos);
      std::string value = line.substr(pos + 2);  // Skip ": "
      headers[key] = value;
    }
  }
  validateHeader();
}

std::string WebServer::HttpHeader::operator[](const string &headerName) {
  return headers[headerName];
}

bool WebServer::HttpHeader::validateHeader() {
  // TODO: Implement header validation
  return true;
}

std::vector<std::string> WebServer::HttpHeader::getHeaderNames() {
  std::vector<std::string> headerNames;
  for (auto &header : headers) {
    headerNames.push_back(header.first);
  }
  return headerNames;
}

WebServer::HttpHeader::HttpHeader(
    const std::map<std::string, std::string> &headers)
    : headers(headers) {}

void WebServer::HttpHeader::setHeader(const string &headerName,
                                      const string &headerValue) {
  headers[headerName] = headerValue;
}

bool WebServer::HttpHeader::hasHeader(const string &headerName) {
  return headers.find(headerName) != headers.end();
}
