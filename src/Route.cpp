//
// Created by nyansapo on 29.2.24.
//

#include "Route.h"

#include <utility>

Path WebServer::Route::getPath() const { return path; }

std::string WebServer::Route::getMethod() const { return method; }

std::string WebServer::Route::getFileName() const {
  if (!fileName.has_value()) {
    throw std::runtime_error("File name is not set");
  }
  return fileName.value();
}

bool WebServer::Route::hasFileName() const { return fileName.has_value(); }

bool WebServer::Route::hasHandler() const { return handler.has_value(); }

WebServer::Route::Route(std::string path, std::string method,
                        std::optional<std::string> fileName,
                        std::optional<RequestHandler> handler)
    : path(Path(std::move(path))),
      method(std::move(method)),
      fileName(std::move(fileName)),
      handler(std::move(handler))
      {}

WebServer::RequestHandler WebServer::Route::getHandler() const {
  if (!handler.has_value()) {
    throw std::runtime_error("Handler is not set");
  }
  return (const std::function<const void(const WebServer::HttpRequest &,
                                         const WebServer::HttpResponse &)> &)
      handler.value();
}

bool WebServer::Route::operator==(const WebServer::Route &other) const {
  return path == other.path && method == other.method;
}
