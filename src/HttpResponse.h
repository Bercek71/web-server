//
// Created by bercek on 17.3.24.
//

#ifndef PROJEKT_HTTPRESPONSE_H
#define PROJEKT_HTTPRESPONSE_H

#include <string>

#include "../lib/json.hpp"
#include "ContentType.h"
#include "HttpHeader.h"

using namespace nlohmann;

namespace WebServer {

class HttpResponse {
 private:
  int statusCode = -1;
  std::string statusMessage;
  std::string body;
  HttpHeader headers;
  ContentType contentType = ContentType::None;
  bool locked = false;
  void prepareHeaders();
  static ContentType convertFileMimeType(string& path);

 public:
  HttpResponse() = default;

  void send(std::string body);

  void sendJSON(json& body);

  void sendFile(std::string path);

  HttpResponse status(int statusCode, std::string statusMessage);
  HttpResponse status(int statusCode);

  [[nodiscard]] bool isLocked() const;

  [[nodiscard]] int getStatusCode() const;

  [[nodiscard]] std::string getStatusMessage() const;

  [[nodiscard]] std::string getBody() const;

  [[nodiscard]] HttpHeader getHeaders() const;

  void setContentType(ContentType contentType);

  std::string toString();
};

}  // namespace WebServer

#endif  // PROJEKT_HTTPRESPONSE_H
