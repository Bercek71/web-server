//
// Created by bercek on 17.3.24.
//

#include "HttpRequest.h"

#include <utility>

bool WebServer::HttpRequest::hasHeader(const string &headerName) {
  return headers.getHeader(headerName).empty();
}

json WebServer::HttpRequest::getBody() { return body; }

std::string WebServer::HttpRequest::getProtocol() { return protocol; }

std::string WebServer::HttpRequest::getPath() { return path; }

std::string WebServer::HttpRequest::getMethod() { return method; }

std::string WebServer::HttpRequest::operator[](const string &key) {
  return headers.getHeader(key);
}



WebServer::HttpRequest::HttpRequest(std::string method, std::string path,
                                    std::string protocol,
                                    WebServer::HttpHeader headers,
                                    json body)
    : method(std::move(method)),
      path(std::move(path)),
      protocol(std::move(protocol)),
      headers(std::move(headers)),
      body(std::move(body)) {}

WebServer::HttpRequest WebServer::HttpRequest::parseRequest(
    const string &requestString) {
  std::istringstream iss(requestString);
  std::string line;
  std::string headerString;
  std::string body;
  WebServer::HttpHeader headers;
  std::map<std::string, std::string> headerMap;
  std::string method, path, protocol;

  // Parsing request line
  std::getline(iss, line);
  std::istringstream reqLine(line);
  reqLine >> method >> path >> protocol;

  // Parsing other headers
  while (std::getline(iss, line)) {
    if (line.empty()) break;
    size_t pos = line.find(": ");
    if (pos != std::string::npos) {
      std::string key = line.substr(0, pos);
      std::string value = line.substr(pos + 2);
      // Remove last char
      value = value.substr(0, value.size() - 1);
      std::transform(key.begin(), key.end(), key.begin(),
                     [](unsigned char c) { return std::tolower(c); });
      headerMap[key] = value;
    }
  }

  // Parsing body
  while (std::getline(iss, line)) {
    body += line + "\n";
  }
  body += line;
  //    Logger::debug("Body: " + body);
  //    Logger::debug("ISS " + iss.str());
  HttpHeader headerParams(headerMap);
  if (headerParams.hasHeader("content-type")) {
    ContentType contentT =
        stringToContentType(headerParams.getHeader("content-type"));
    if (contentT == ContentType::APPLICATION_JSON) {
      json j = json::parse(body);
      return {method, path, protocol, headerParams, j};
    }
    if (contentT == ContentType::APPLICATION_X_WWW_FORM_URLENCODED) {
      std::istringstream iss(body);
      std::string line;
      std::map<std::string, std::string> bodyMap;
      while (std::getline(iss, line)) {
        size_t last_pos = 0;
        for (size_t i = 0; i <= std::count(line.begin(), line.end(), '&');
             i++) {
          size_t pos = line.find('=', last_pos);
          if (pos != std::string::npos) {
            std::string key = line.substr(last_pos, pos - last_pos);
            size_t pos2 = line.find('&', last_pos);
            std::string value = line.substr(pos + 1, pos2 - pos - 1);
            bodyMap[key] = value;
            last_pos = pos2 + 1;
          }
        }
      }
      json j = bodyMap;
      return {method, path, protocol, headerParams, j};
    }
  }
  return {method, path, protocol, headerParams, body};
}

bool WebServer::HttpRequest::isEmpty() const { return empty; }

WebServer::HttpRequest::HttpRequest(const std::string &rawRequest) {
  if (rawRequest.empty()) {
    empty = true;
    return;
  }
  *this = parseRequest(rawRequest);
}

std::string WebServer::HttpRequest::getParam(const string &paramName) {
    return dynamicParams[paramName];
}

void WebServer::HttpRequest::setParams(const std::map<std::string, std::string> &params) {
    dynamicParams = params;
}
